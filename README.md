# mon88

Modifications of the MON88 monitor developed by HT-Lab

This monitor was originally released to accompany the [CPU86 soft-8088 core](http://www.ht-lab.com/cpu86.htm).  Its limited dependencies (compared with, say, DEBUG.COM) makes it a reasonable target for an "embedded ROM debugger".

## Changes from original
* There were several stub interrupt handlers, which are unnecessary when running with a PC-compatible BIOS.  These are removed.  The IO now uses regular BIOS calls.

* The original MON88 was designed to compile against the A86 commercial assembler.  This is tweaked to try to build against NASM.  There may be subtle inconsistencies or bugs

* Useless commands (loading file via a nonexistent serial link, quit) have been disabled

## Things likely needing testing

* How to handle returning control to the monitor, and breakpoints.  Since there's no easy way to load in code, testing involves injecting a few lines of pre-assembled code.

## Things that would be nice to add

* Better input filtering-- type a letter into a field expecting a number, and who knows what happens next.  Maybe even "backspace" support

* A way to break long dumps/disassemblies.

## License
"The CPU86 source code is licensed under the GNU General Public License v2 or v3".  The MON88 source specifically calls out v2.


